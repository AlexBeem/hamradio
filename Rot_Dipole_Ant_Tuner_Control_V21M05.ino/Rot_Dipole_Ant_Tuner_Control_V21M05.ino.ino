/********************************************************************/
/* Rotary Dipole Antenna Tuner Control via H-Bridge L298n           */
/* using 2x16 LCD Dot Matix display                                 */
/*                                                                  */
/* DF7XH / R. Buese                                                 */
/* Jan. 2015                                                        */
/********************************************************************/
/* Modify
 *  UA6EM op.Victor
 *  08.02.2020 add 3-BAND and STRUCT in EEPROM  
 */
/* LCD circuit
 * LCD RS pin to digital pin 0
 * LCD Enable pin to digital pin 1
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
 * Для подключения дисплея по шине I2C (A4-A5 UNO,nano)
 * размаркируйте #define LCD1602_I2C
 */
//#define NANO

#ifndef NANO
#define BUTTON 18   // Если UNO используем для кнопки записи пин А4
#define BUZZER 19   // Если UNO используем для зуммера пин А5
#else
//#define LCD1602_I2C
#ifdef LCD1602_I2C
#define BUTTON 5 // Если LCD1602 I2C используем для кнопки записи пин D5
#define BUZZER 6 // Если LCD1602 I2C используем для зуммера пин D6
#else
#define BUTTON 18 // используем для кнопки записи пин А4
#define BUZZER 19 // используем для зуммера пин А5
#endif
#endif

#define DEBUG

#define DIT   100  
#define DASH  300
#define PAUSE 100
 
// EEPROM адресация
#include <EEPROM.h>
#include <util/atomic.h>

struct MyLC {
uint8_t  flagLC;  
int posn_cmd_l_160=640;
int posn_cmd_l_80=640;
int posn_cmd_l_40=640;
int posn_cmd_l_30=640;
int posn_cmd_l_20=184;
int posn_cmd_l_17=972;
int posn_cmd_l_15=373;
int posn_cmd_l_12=180;
int posn_cmd_l_10=156;

int posn_cmd_c_160=180;
int posn_cmd_c_80=180;
int posn_cmd_c_40=180;
int posn_cmd_c_30=180;
int posn_cmd_c_20=582;
int posn_cmd_c_17=361;
int posn_cmd_c_15=247;
int posn_cmd_c_12=277;
int posn_cmd_c_10=387;
uint8_t f6 = 0xf6;
uint8_t ff = 0x55;
};
MyLC tuner;
int eeAddress = 5;

/*** Обработчик кнопки ***/
unsigned long mill; // переменная под  millis()
//------Cl_Btn----------------------
enum {sbNONE = 0, sbClick, sbLong}; /*состояние не изменилось/клик/долгое нажатие*/
class Cl_Btn {
  protected:
    const byte pin;
    byte state;
    bool bounce = 0;
    bool btn = 1, oldBtn;
    unsigned long past;
    const uint32_t time = 500 ;
    bool flag = 0;
    uint32_t past_flag = 0 ;
  public:
    Cl_Btn(byte p): pin(p) {}
    /*инициализация-вставить в setup()*/
    void init() {
      pinMode(pin, INPUT_PULLUP);
    }
    /*работа-вставить в loop()*/
    void run() {
      state = sbNONE;
      bool newBtn = digitalRead(pin);
      if (!bounce && newBtn != btn) {
        bounce = 1;
        past = mill;
      }
      if (bounce && mill - past >= 10) {
        bounce = 0 ;
        oldBtn = btn;
        btn = newBtn;
        if (!btn && oldBtn) {
          flag = 1;
          past_flag = mill;
        }
        if (!oldBtn && btn && flag && mill - past_flag < time ) {
          flag = 0;
          state = sbClick;
        }
      }
      if (flag && mill - past_flag >= time ) {
        flag = 0;
        state = sbLong;
      }
    }
    byte read() {
      return state;
    }
};
Cl_Btn BtnR(/*пин*/BUTTON); //Экземпляр обработчика для кнопки


/**********************Arduino Pin Definition************************/

uint8_t pwm_L = 11;           // PWM (Enable) output pin for L motor
uint8_t up_L = 13;            // Direction Up (I1) output pin for L motor
uint8_t down_L  = 12;         // Direction Down (I2) output pin for L motor
uint8_t pwm_C = 6;            // PWM (Enable) output pin for C motor
uint8_t up_C = 8;             // Direction Up (I1) output pin for C motor
uint8_t down_C  = 7;          // Direction Down (I2) output pin for C motor
uint8_t ana_pin_L = 1;        // Analog input pin L motor position
uint8_t ana_pin_C = 0;        // Analog input pin C motor position
uint8_t ana_band = 2;         // Analog input pin for band selection switch
uint8_t ana_pin_fine_tune_C = 3;      // Analog input pin for fine tuning C motor PB switch
uint8_t pin_fine_tune_L_up = 10;      // Discrete input pin for fine tuning L motor PB switch
uint8_t pin_fine_tune_L_dwn = 9;      // Discrete input pin for fine tuning L motor PB switch

/**********************Arduino Constant Definition*******************/

// Thresholds for C / L motor control
#define LOW_SPEED_THRES 20   // Low speed threshold
//#define LOW_SPEED_THRES 10   // Low speed threshold
#define TARGET_THRES_L 0     // Target positioning threshold
#define TARGET_THRES_C 0     // Target positioning threshold

// Commanded positions for L motor depending on band
#define POSN_CMD_L_160 640 // Commanded position L motor for 160m band  // Добавлены диапазоны 40-80-160
#define POSN_CMD_L_80 640 // Commanded position L motor for 80m band    // параметры не измерялись
#define POSN_CMD_L_40 640 // Commanded position L motor for 40m band

#define POSN_CMD_L_30 640 // Commanded position L motor for 30m band
#define POSN_CMD_L_20 184 // Commanded position L motor for 20m band
#define POSN_CMD_L_17 972 // Commanded position L motor for 17m band
#define POSN_CMD_L_15 373 // Commanded position L motor for 15m band
#define POSN_CMD_L_12 180 // Commanded position L motor for 12m band
#define POSN_CMD_L_10 156 // Commanded position L motor for 10m band

// Commanded positions for C motor depending on band
#define POSN_CMD_C_160 180 // Commanded position C motor for 160m band  // Добавлены диапазоны 40-80-160
#define POSN_CMD_C_80 180 // Commanded position C motor for 80m band    // параметры не измерялись
#define POSN_CMD_C_40 180 // Commanded position C motor for 40m band

#define POSN_CMD_C_30 180 // Commanded position C motor for 30m band
#define POSN_CMD_C_20 582 // Commanded position C motor for 20m band
#define POSN_CMD_C_17 361 // Commanded position C motor for 17m band
#define POSN_CMD_C_15 247 // Commanded position C motor for 15m band
#define POSN_CMD_C_12 277 // Commanded position C motor for 12m band
#define POSN_CMD_C_10 387 // Commanded position C motor for 10m band

// PWM settings
// PWM 0 == Duty Cycle = 0%  255 == Duty Cycle = 100%
#define PWM_CMD_HIGH_SPEED_L 190 // ~75% duty cycle for high speed operation
#define PWM_CMD_LOW_SPEED_L  90  // ~30% duty cycle for low speed operation
#define PWM_CMD_HIGH_SPEED_C 190 // ~75% duty cycle for high speed operation
#define PWM_CMD_LOW_SPEED_C  90  // ~30% duty cycle for low speed operation

// Defines the number of loops before the LCD display will be updated
#define DISPLAY_LOOP_COUNTER_MAX 6

#ifdef LCD1602_I2C
#include <Wire.h>
#include <LCD_1602_RUS.h>  // <a href="https://github.com/ssilver2007/LCD_1602_RUS" rel="nofollow">https://github.com/ssilver2007/LCD_1602_RUS</a>
LCD_1602_RUS lcd(0x3F, 20, 4); // у меня такой ;-)
#else
// Include the library code:
#include <LiquidCrystal.h>
// Initialize the library with the numbers of the interface pins
LiquidCrystal lcd(0, 1, 5, 4, 3, 2);
#endif

/**********************Variable Declaration***************************/

int posn_cmd_L = 0;
int posn_cmd_C = 0;

boolean in_motion_L = false;
boolean in_motion_C = false;

boolean power_up = true;

int posn_L = 0;          // Input position (0..1000) i.e. (0..5V)  for L motor
int posn_L_0 = 0;        // Past values of input position
int posn_L_1 = 0;
int posn_L_2 = 0;
int posn_L_3 = 0;
int posn_L_4 = 0;
int posn_L_5 = 0;
int posn_L_6 = 0;

int posn_C = 0;          // Input position (0..1000) i.e. (0..5V)  for C motor
int posn_C_0 = 0;        // Past values of input position
int posn_C_1 = 0;
int posn_C_2 = 0;
int posn_C_3 = 0;
int posn_C_4 = 0;
int posn_C_5 = 0;
int posn_C_6 = 0;

int posn_diff_L = 0;
int posn_diff_C = 0;

int band = 0;
int band_0 = 0;        // Past values of band selection switch
int band_1 = 0;
int band_2 = 0;
int band_3 = 0;
int band_4 = 0;

int band_sel = 0;
int band_sel_old = 0;

int fine_tune_C = 512;
int fine_tune_C_0 = 512;        // Past values of band selection switch
int fine_tune_C_1 = 512;
int fine_tune_C_2 = 512;
int fine_tune_C_3 = 512;
int fine_tune_C_4 = 512;

int display_loop_counter = DISPLAY_LOOP_COUNTER_MAX;
void loadLC(){
// Процедура чтения данных из EEPROM  
  EEPROM.get(eeAddress, tuner);  // Получить данные о настройках LC
}

void saveLC(){
// Процедура записи данных в EEPROM 
  EEPROM.put(eeAddress, tuner);  
}

void readButtons(){
band_0 = analogRead(ana_band);  
}

void selectBand(){
  if (((band >= 0) && (band < 65)) && ((band_1 >= 0) && (band_1 < 65)))   // 160m band selected
  {
   band_sel = 160;
    if(tuner.flagLC == 88 && tuner.posn_cmd_c_160 > 0 && tuner.posn_cmd_c_160 < 1023){
    posn_cmd_L = tuner.posn_cmd_l_160; // Взять из EEPROM
    posn_cmd_C = tuner.posn_cmd_c_160;
    }else{
    posn_cmd_L = POSN_CMD_L_160;       // Иначе берём из предустановок скетча
    posn_cmd_C = POSN_CMD_C_160;
         }   
  } else
    if (((band >= 65) && (band < 130)) && ((band_1 >= 65) && (band_1 < 130)))   // 80m band selected
    {
     band_sel = 80;
      if(tuner.flagLC == 88 && tuner.posn_cmd_c_80 > 0 && tuner.posn_cmd_c_80 < 1023){
      posn_cmd_L = tuner.posn_cmd_l_80; // Взять из EEPROM
      posn_cmd_C = tuner.posn_cmd_c_80;
      }else{
      posn_cmd_L = POSN_CMD_L_80;       // Иначе берём из предустановок скетча
      posn_cmd_C = POSN_CMD_C_80;
           }   
    } else
      if (((band >= 130) && (band < 190)) && ((band_1 >= 130) && (band_1 < 190)))   // 40m band selected
      {
       band_sel = 40;
        if(tuner.flagLC == 88 && tuner.posn_cmd_c_40 > 0 && tuner.posn_cmd_c_40 < 1023){
        posn_cmd_L = tuner.posn_cmd_l_40; // Взять из EEPROM
        posn_cmd_C = tuner.posn_cmd_c_40;
        }else{
        posn_cmd_L = POSN_CMD_L_40;       // Иначе берём из предустановок скетча
        posn_cmd_C = POSN_CMD_C_40;
             }   
      } else
        if (((band >= 190) && (band < 250)) && ((band_1 >= 190) && (band_1 < 250)))   // 30m band selected
        {
         band_sel = 30;
          if(tuner.flagLC == 88 && tuner.posn_cmd_c_30 > 0 && tuner.posn_cmd_c_30 < 1023){
          posn_cmd_L = tuner.posn_cmd_l_30; // Взять из EEPROM
          posn_cmd_C = tuner.posn_cmd_c_30;
          }else{
          posn_cmd_L = POSN_CMD_L_30;       // Иначе берём из предустановок скетча
          posn_cmd_C = POSN_CMD_C_30;
             }     
        } else
          if (((band >= 251) && (band < 416)) && ((band_1 >= 251) && (band_1 < 416)))    // 20m band selected
          {
           band_sel = 20;
           if(tuner.flagLC == 88 && tuner.posn_cmd_c_20 > 0 && tuner.posn_cmd_c_20 < 1023){
           posn_cmd_L = tuner.posn_cmd_l_20; // Взять из EEPROM
           posn_cmd_C = tuner.posn_cmd_c_20;
           }else{
           posn_cmd_L = POSN_CMD_L_20;       // Иначе берём из предустановок скетча
           posn_cmd_C = POSN_CMD_C_20;
                }     
          } else
            if (((band >= 416) && (band < 603)) && ((band_1 >= 416) && (band_1 < 603)))    // 17m band selected
            {
             band_sel = 17;
              if(tuner.flagLC == 88 && tuner.posn_cmd_c_17 > 0 && tuner.posn_cmd_c_17 < 1023){
              posn_cmd_L = tuner.posn_cmd_l_17; // Взять из EEPROM
              posn_cmd_C = tuner.posn_cmd_c_17;
              }else{
              posn_cmd_L = POSN_CMD_L_17;       // Иначе берём из предустановок скетча
              posn_cmd_C = POSN_CMD_C_17;
                   } 
            } else
              if (((band >= 603) && (band < 740)) && ((band_1 >= 603) && (band_1 < 740)))    // 15m band selected
              {
              band_sel = 15;
               if(tuner.flagLC == 88 && tuner.posn_cmd_c_15 > 0 && tuner.posn_cmd_c_15 < 1023){
               posn_cmd_L = tuner.posn_cmd_l_15; // Взять из EEPROM
               posn_cmd_C = tuner.posn_cmd_c_15;
               }else{
               posn_cmd_L = POSN_CMD_L_15;       // Иначе берём из предустановок скетча
               posn_cmd_C = POSN_CMD_C_15;
                    } 
              } else
                if (((band >= 740) && (band < 810)) && ((band_1 >= 740) && (band_1 < 810)))    // 12m band selected
                {
                 band_sel = 12;
                  if(tuner.flagLC == 88 && tuner.posn_cmd_c_12 > 0 && tuner.posn_cmd_c_12 < 1023){
                  posn_cmd_L = tuner.posn_cmd_l_12; // Взять из EEPROM
                  posn_cmd_C = tuner.posn_cmd_c_12;
                  }else{
                  posn_cmd_L = POSN_CMD_L_12;       // Иначе берём из предустановок скетча
                  posn_cmd_C = POSN_CMD_C_12;
                       } 
                 } else
                   if (((band >= 810) && (band < 900)) && ((band_1 >= 810) && (band_1 < 900)))    // 10m band selected
                   {
                    band_sel = 10;
                     if(tuner.flagLC == 88 && tuner.posn_cmd_c_10 > 0 && tuner.posn_cmd_c_10 < 1023){
                     posn_cmd_L = tuner.posn_cmd_l_10; // Взять из EEPROM
                     posn_cmd_C = tuner.posn_cmd_c_10;
                     }else{
                     posn_cmd_L = POSN_CMD_L_10;       // Иначе берём из предустановок скетча
                     posn_cmd_C = POSN_CMD_C_10;
                          } 
                   } 
      if ((band_sel != band_sel_old) && (power_up == false))
  {
    in_motion_L = true;
    in_motion_C = true;
  }

  band_sel_old = band_sel;
  power_up = false;               
                   
} // END selectBand

void readLC(){
  posn_L_0 = analogRead(ana_pin_L); // Read position L motor

  // Map position sensor value to 0...1000
  posn_L_0 = map(posn_L_0, 0, 1023, 0, 1000);

  // Position sensor filter
  posn_L = (posn_L_0 + posn_L_1 + posn_L_2 + posn_L_3 + posn_L_4 + posn_L_5 + posn_L_6)/7;
  posn_L_6 = posn_L_5;
  posn_L_5 = posn_L_4;
  posn_L_4 = posn_L_3;
  posn_L_3 = posn_L_2;
  posn_L_2 = posn_L_1;
  posn_L_1 = posn_L_0;

  // Caution C Poti rotates only 1/2 turn, i.e. max. voltage is 4.5V. This related to 5V results in a factor 1.112
  posn_C_0 = 1.11 * analogRead(ana_pin_C); // Read position C motor

  // Map position sensor value to 0...1000
  posn_C_0 = map(posn_C_0, 0, 1023, 0, 1000);

  // Position sensor filter
  posn_C = (posn_C_0 + posn_C_1 + posn_C_2 + posn_C_3 + posn_C_4 + posn_C_5 + posn_C_6)/7;
  posn_C_6 = posn_C_5;
  posn_C_5 = posn_C_4;
  posn_C_4 = posn_C_3;
  posn_C_3 = posn_C_2;
  posn_C_2 = posn_C_1;
  posn_C_1 = posn_C_0;

  // Compute position difference between current position and command
  posn_diff_L = posn_cmd_L - posn_L;
  posn_diff_C = posn_cmd_C - posn_C;
}

void setMotion(){
  // Motion for C motor and L motor requested (motion of C motor has priority over motion of L motor) or
  // Motion only for C motor requested
  if (((in_motion_C == true) && (in_motion_L == true)) || ((in_motion_C == true) && (in_motion_L == false)))
  {
   // Reset direction and PWM output of L motor (for safety)
   digitalWrite(up_L, LOW);
   digitalWrite(down_L, LOW);
   analogWrite(pwm_L, 0);

   // Control motion of C motor depending on C motor position
   if (posn_diff_C > LOW_SPEED_THRES)
   {
      digitalWrite(up_C, HIGH); // Set direction of C motor to Up
      digitalWrite(down_C, LOW);
      analogWrite(pwm_C, PWM_CMD_HIGH_SPEED_C);  // Set PWM output to High speed
      lcd.setCursor(10, 1);
      lcd.print(">>");
   }
   if (posn_diff_C < -LOW_SPEED_THRES)
   {
      digitalWrite(up_C, LOW); // Set direction of C motor to Down
      digitalWrite(down_C, HIGH);
      analogWrite(pwm_C, PWM_CMD_HIGH_SPEED_C);  // Set PWM output to High speed
      lcd.setCursor(10, 1);
      lcd.print(">>");
   }
   if ((posn_diff_C <= LOW_SPEED_THRES) && (posn_diff_C > TARGET_THRES_C))
   {
      digitalWrite(up_C, HIGH); // Set direction of C motor to Up
      digitalWrite(down_C, LOW);
      analogWrite(pwm_C, PWM_CMD_LOW_SPEED_C);  // Set PWM output to Low speed
      lcd.setCursor(10, 1);
      lcd.print("> ");
   }
   if ((posn_diff_C >= -LOW_SPEED_THRES) && (posn_diff_C < -TARGET_THRES_C))
   {
      digitalWrite(up_C, LOW); // Set direction of C motor to Down
      digitalWrite(down_C, HIGH);
      analogWrite(pwm_C, PWM_CMD_LOW_SPEED_C);  // Set PWM output to Low speed
      lcd.setCursor(10, 1);
      lcd.print("> ");
   }
   if ((posn_diff_C <= TARGET_THRES_C) && (posn_diff_C >= -TARGET_THRES_C))
   {
      digitalWrite(up_C, LOW); // Reset direction of C motor
      digitalWrite(down_C, LOW);
      analogWrite(pwm_C, 0);  // Reset PWM output
      lcd.setCursor(10, 1);
      lcd.print("  ");
      in_motion_C = false;
   }
  }
  else
    // Motion only for L motor requested
    if ((in_motion_C == false) && (in_motion_L == true))
    {
     // Reset direction and PWM output of C motor (for safety)
     digitalWrite(up_C, LOW);
     digitalWrite(down_C, LOW);
     analogWrite(pwm_C, 0);

     // Control motion of L motor depending on L motor position
     if (posn_diff_L > LOW_SPEED_THRES)
     {
        digitalWrite(up_L, HIGH); // Set direction of L motor to Up
        digitalWrite(down_L, LOW);
        analogWrite(pwm_L, PWM_CMD_HIGH_SPEED_L);  // Set PWM output to High speed
        lcd.setCursor(4, 1);
        lcd.print("<<");
     }
     if (posn_diff_L < -LOW_SPEED_THRES)
     {
        digitalWrite(up_L, LOW); // Set direction of L motor to Down
        digitalWrite(down_L, HIGH);
        analogWrite(pwm_L, PWM_CMD_HIGH_SPEED_L);  // Set PWM output to High speed
        lcd.setCursor(4, 1);
        lcd.print("<<");
     }
     if ((posn_diff_L <= LOW_SPEED_THRES) && (posn_diff_L > TARGET_THRES_L))
     {
        digitalWrite(up_L, HIGH); // Set direction of L motor to Up
        digitalWrite(down_L, LOW);
        analogWrite(pwm_L, PWM_CMD_LOW_SPEED_L);  // Set PWM output to Low speed
        lcd.setCursor(4, 1);
        lcd.print(" <");
     }
     if ((posn_diff_L >= -LOW_SPEED_THRES) && (posn_diff_L < -TARGET_THRES_L))
     {
        digitalWrite(up_L, LOW); // Set direction of L motor to Down
        digitalWrite(down_L, HIGH);
        analogWrite(pwm_L, PWM_CMD_LOW_SPEED_L);  // Set PWM output to Low speed
        lcd.setCursor(4, 1);
        lcd.print(" <");
     }
     if ((posn_diff_L <= TARGET_THRES_L) && (posn_diff_L >= -TARGET_THRES_L))
     {
        digitalWrite(up_L, LOW); // Reset direction of L motor
        digitalWrite(down_L, LOW);
        analogWrite(pwm_L, 0);  // Reset PWM output
        lcd.setCursor(4, 1);
        lcd.print("  ");
        in_motion_L = false;
     }
    }
    else
      // No motion requested
      {
       // Reset direction and PWM output of L motor (for safety)
       digitalWrite(up_L, LOW);
       digitalWrite(down_L, LOW);
       analogWrite(pwm_L, 0);

       // Reset direction and PWM output of C motor (for safety)
       digitalWrite(up_C, LOW);
       digitalWrite(down_C, LOW);
       analogWrite(pwm_C, 0);

       // Fine tune is allowed only, neither C motor nor L motor motion is requested
       fine_tune_C_0 = analogRead(ana_pin_fine_tune_C); // Read position of fine tuning C motor PB switch

       // Fine tuning C motor PB switch filter
       fine_tune_C = (fine_tune_C_0 + fine_tune_C_1 + fine_tune_C_2 + fine_tune_C_3+ fine_tune_C_4)/5;
       fine_tune_C_4 = fine_tune_C_3;
       fine_tune_C_3 = fine_tune_C_2;
       fine_tune_C_2 = fine_tune_C_1;
       fine_tune_C_1 = fine_tune_C_0;

       if (fine_tune_C > 768) // Fine tuning C motor PB switch commands the C motor to Up
       {
           digitalWrite(up_C, HIGH); // Set direction of C motor to Up
           digitalWrite(down_C, LOW);
           analogWrite(pwm_C, PWM_CMD_HIGH_SPEED_C);  // Set PWM output to High speed

           delay(100);  // Allow C motor to move (fine tune)

           digitalWrite(up_C, LOW); // Reset direction of C motor
           digitalWrite(down_C, LOW);
           analogWrite(pwm_C, 0);  // Reset PWM output
       } else
          if (fine_tune_C < 256) // Fine tuning C motor PB switch commands the C motor to Down
          {
             digitalWrite(up_C, LOW); // Set direction of C motor to Down
             digitalWrite(down_C, HIGH);
             analogWrite(pwm_C, PWM_CMD_HIGH_SPEED_C);  // Set PWM output to High speed

             delay(100);  // Allow C motor to move (fine tune)

             digitalWrite(up_C, LOW); // Reset direction of C motor
             digitalWrite(down_C, LOW);
             analogWrite(pwm_C, 0);  // Reset PWM output
           }

        if (digitalRead(pin_fine_tune_L_up) == LOW) // Fine tuning L motor PB switch commands the L motor to Up
        {
           digitalWrite(up_L, HIGH); // Set direction of L motor to Up
           digitalWrite(down_L, LOW);
           analogWrite(pwm_L, PWM_CMD_HIGH_SPEED_L);  // Set PWM output to High speed

           delay(200);  // Allow L motor to move (fine tune)

           digitalWrite(up_L, LOW); // Reset direction of L motor
           digitalWrite(down_L, LOW);
           analogWrite(pwm_L, 0);  // Reset PWM output
        } else
           if (digitalRead(pin_fine_tune_L_dwn) == LOW) // Fine tuning L motor PB switch commands the L motor to Dwn
           {
             digitalWrite(up_L, LOW); // Set direction of L motor to Down
             digitalWrite(down_L, HIGH);
             analogWrite(pwm_L, PWM_CMD_HIGH_SPEED_L);  // Set PWM output to High speed

             delay(200);  // Allow L motor to move (fine tune)

             digitalWrite(up_L, LOW); // Reset direction of L motor
             digitalWrite(down_L, LOW);
             analogWrite(pwm_L, 0);  // Reset PWM output
           }

      }

  delay(30);

#ifdef DEBUG            
  // Output on serial monitor
  Serial.print(band_sel);
  Serial.print("m");
  Serial.print("\t");
  Serial.print(posn_L);
  Serial.print("\t");
  Serial.print(posn_cmd_L);
  Serial.print("\t");
  Serial.print(posn_C);
  Serial.print("\t");
  Serial.println(posn_cmd_C);
#endif

  if (display_loop_counter >= DISPLAY_LOOP_COUNTER_MAX)
  {

  // Print a fixed message to the LCD.
  lcd.setCursor(8, 0);
  lcd.print("m");

  // set the cursor to column 6, line 0
  lcd.setCursor(6, 0);
  // print band_sel
  lcd.print(band_sel);

  // Clear specific column, line on LCD.
  lcd.setCursor(3, 0);
  lcd.print(" ");

  // set the cursor to column 0, line 0
  lcd.setCursor(0, 0);
  // clear
  lcd.print("    ");
  // re-position cursor
  lcd.setCursor(0, 0);
  // print posn_cmd_L
  lcd.print(posn_cmd_L);

  // set the cursor to column 0, line 1
  lcd.setCursor(0, 1);
  // clear
  lcd.print("    ");
  // re-position cursor
  lcd.setCursor(0, 1);
  // print posn_L
  lcd.print(posn_L);

  // set the cursor to column 12, line 0
  lcd.setCursor(12, 0);
  // clear
  lcd.print("    ");
  // re-position cursor
  lcd.setCursor(12, 0);
  // print posn_cmd_C
  lcd.print(posn_cmd_C);

  // set the cursor to column 12, line 1
  lcd.setCursor(12, 1);
  // clear
  lcd.print("    ");
  // re-position cursor
  lcd.setCursor(12, 1);
  // print posn_C
  lcd.print(posn_C);

  display_loop_counter = 0;
  } else
  {
    display_loop_counter ++;
  }
}

void toneSaveeprom(){
   int i=0; 
   do { 
     tone(BUZZER,1000,DIT);
     delay(DIT+PAUSE);
     tone(BUZZER,1000,DASH);
     delay(DASH+PAUSE);
     tone(BUZZER,1000,DIT);
     delay(DIT+PAUSE);
     delay(2*PAUSE); i++;} while(i<3);
     delay(2*PAUSE);
}

void toneS() {
  int i=0; do{ tone(BUZZER,1000,DIT); delay(DIT+PAUSE);i++;} while(i<3); delay(2*PAUSE);
}

void saveCMD(){
  if(band_sel==10){
  tuner.posn_cmd_l_10 = posn_L; 
  tuner.posn_cmd_c_10 = posn_C;
  }
   if(band_sel==12){
   tuner.posn_cmd_l_12 = posn_L; 
   tuner.posn_cmd_c_12 = posn_C;
   }               
    if(band_sel==15){
    tuner.posn_cmd_l_15 = posn_L; 
    tuner.posn_cmd_c_15 = posn_C;
    } 
     if(band_sel==20){
     tuner.posn_cmd_l_20 = posn_L; 
     tuner.posn_cmd_c_20 = posn_C;
     } 
      if(band_sel==30){
      tuner.posn_cmd_l_30 = posn_L; 
      tuner.posn_cmd_c_30 = posn_C;
      }       
       if(band_sel==40){
       tuner.posn_cmd_l_40 = posn_L; 
       tuner.posn_cmd_c_40 = posn_C;
       }       
        if(band_sel==80){
        tuner.posn_cmd_l_80 = posn_L; 
        tuner.posn_cmd_c_80 = posn_C;
        }       
         if(band_sel==160){
         tuner.posn_cmd_l_160 = posn_L; 
         tuner.posn_cmd_c_160 = posn_C;
         }   
}

void setup() {
  pinMode(pwm_L, OUTPUT);
  pinMode(up_L, OUTPUT);
  pinMode(down_L, OUTPUT);
  pinMode(pwm_C, OUTPUT);
  pinMode(up_C, OUTPUT);
  pinMode(down_C, OUTPUT);
  pinMode(pin_fine_tune_L_up, INPUT_PULLUP); // Discrete input pin for fine tuning L motor PB switch
  pinMode(pin_fine_tune_L_dwn,INPUT_PULLUP); // Discrete input pin for fine tuning L motor PB switch
  pinMode(BUZZER,OUTPUT);
  
#ifdef DEBUG
  Serial.begin(115200);
#endif

#ifdef LCD1602_I2C
  lcd.begin();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Rot.Dipole Cont.");
  lcd.setCursor(0, 3);
  lcd.print("V-01b      UA6EM"); 
  delay(3000);
  lcd.clear();
#else
  // Set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // Display intro
  lcd.setCursor(0, 0);
  lcd.print("Rot.Dipole Cont.");
  lcd.setCursor(0, 1);
  lcd.print("V-01b      UA6EM");

  delay(3000);
  lcd.clear();
#endif
  uint8_t value = EEPROM.read(eeAddress); // Получить флаг
  if(value == 88){
  loadLC();
  }
  BtnR.init();  // Подключили кнопку записи в EEPROM
}

void loop() {
  BtnR.run(); 
  readButtons();

#ifdef DEBUG
Serial.print("band = ");
Serial.println(band);
Serial.print("band_1 = ");
Serial.println(band_1);
delay(300);
#endif

selectBand();
readLC();
setMotion();
if (BtnR.read() == sbClick)  { // сохранить настройки в структуру
saveCMD();         
toneS();  
}

if (BtnR.read() == sbLong)   { // сохранить настройки в EEPROM
   EEPROM.put(eeAddress, tuner); 
   toneSaveeprom();
 }
} // END LOOP
  // END PROGRAMM
  
